#include <QCanBus>
#include <QCanBusFrame>
#include <QMessageBox>


#include "widget.h"
#include "ui_widget.h"
//----------------------------------------------------------------------------------------------------
Widget::Widget(QWidget *parent) :
    QWidget(parent),

    m_canDevice(nullptr),
    mTimerId(-1),

    ui(new Ui::Widget)
{

    ui->setupUi(this);

    connect(ui->connectButton,SIGNAL(clicked(bool)),this,SLOT(connectDeviceSlot()));
    connect(ui->disconnectButton,SIGNAL(clicked(bool)),this,SLOT(disconnectDevice()));

    connect(ui->sendDataButton, SIGNAL(clicked(bool)), this, SLOT(sendMessageSlot(void)));

    ui->connectButton->setEnabled(true);
    ui->disconnectButton->setEnabled(false);
    ui->sendDataButton->setEnabled(false);
    ui->receivedMessagesEdit->setEnabled(false);
    ui->connectLabel->setText("DISCONNECTED");
}
//----------------------------------------------------------------------------------------------------
Widget::~Widget()
{
    delete ui;
}
//----------------------------------------------------------------------------------------------------
void Widget::connectDeviceSlot()
{

    if(m_canDevice != nullptr)
        return;

    //Create a Device
    QString errorString;
    m_canDevice = QCanBus::instance()->createDevice("socketcan","can0",&errorString);

    if (!m_canDevice) {

        QMessageBox msgBox;
        msgBox.setText(QString("Error creating socketcan device, reason: %1").arg(errorString));
        msgBox.exec();

        return;
    }

    //Connect Signals
    connect(m_canDevice, &QCanBusDevice::errorOccurred, this, &Widget::receiveError);
    connect(m_canDevice, &QCanBusDevice::framesReceived,this, &Widget::checkMessages);
    connect(m_canDevice, &QCanBusDevice::framesWritten, this, &Widget::framesWritten);


    if (!m_canDevice->connectDevice()) {

        QMessageBox msgBox;
        msgBox.setText(QString("Connection error: %1").arg(m_canDevice->errorString()));
        msgBox.exec();

        delete m_canDevice;
        m_canDevice = nullptr;


    } else {

        //Ok all is running

        ui->connectButton->setEnabled(false);
        ui->disconnectButton->setEnabled(true);
        ui->sendDataButton->setEnabled(true);
        ui->receivedMessagesEdit->setEnabled(true);
        ui->connectLabel->setText("CONNECTED");

    }
}
//----------------------------------------------------------------------------------------------------
void Widget::disconnectDevice()
{
    if (!m_canDevice)
        return;

    m_canDevice->disconnectDevice();
    delete m_canDevice;
    m_canDevice = nullptr;

    ui->connectButton->setEnabled(true);
    ui->disconnectButton->setEnabled(false);
    ui->sendDataButton->setEnabled(false);
    ui->receivedMessagesEdit->setEnabled(false);
    ui->connectLabel->setText("DISCONNECTED");
}
//----------------------------------------------------------------------------------------------------
void Widget::receiveError(QCanBusDevice::CanBusError error)
{
    switch (error) {
    case QCanBusDevice::ReadError:
    case QCanBusDevice::WriteError:
    case QCanBusDevice::ConnectionError:
    case QCanBusDevice::ConfigurationError:
    case QCanBusDevice::UnknownError:
        writeStatusMsg( QString(m_canDevice->errorString()) );
    default:
        break;
    }
}

//----------------------------------------------------------------------------------------------------
QString Widget::frameFlags(const QCanBusFrame &frame)
{
    if (frame.hasBitrateSwitch() && frame.hasErrorStateIndicator())
        return QStringLiteral(" B E ");
    if (frame.hasBitrateSwitch())
        return QStringLiteral(" B - ");
    if (frame.hasErrorStateIndicator())
        return QStringLiteral(" - E ");
    return QStringLiteral(" - - ");
}
//----------------------------------------------------------------------------------------------------
void Widget::checkMessages()
{
    if (!m_canDevice)
        return;

    while (m_canDevice->framesAvailable()) {

        const QCanBusFrame frame = m_canDevice->readFrame();

        QString view;
        if (frame.frameType() == QCanBusFrame::ErrorFrame)
            view = m_canDevice->interpretErrorFrame(frame);
        else
            view = frame.toString();

        const QString time = QString::fromLatin1("%1.%2  ")
                .arg(frame.timeStamp().seconds(), 10, 10, QLatin1Char(' '))
                .arg(frame.timeStamp().microSeconds() / 100, 4, 10, QLatin1Char('0'));

        const QString flags = frameFlags(frame);

        ui->receivedMessagesEdit->append(time + flags + view);
    }
}
//----------------------------------------------------------------------------------------------------
void Widget::framesWritten(qint64 count)
{
   writeStatusMsg(QString("Number of frames written: %1").arg(count));
}
//----------------------------------------------------------------------------------------------------
void Widget::closeEvent(QCloseEvent */*event*/)
{
    disconnectDevice();
}
//----------------------------------------------------------------------------------------------------
QByteArray Widget::dataFromHex(const QString &hex)
{
    QByteArray line = hex.toLatin1();
    line.replace(' ', QByteArray());
    return QByteArray::fromHex(line);
}
//----------------------------------------------------------------------------------------------------
void Widget::sendMessageSlot(){

    if (!m_canDevice)
        return;

    QByteArray writings = dataFromHex(ui->lineEdit->displayText());

    QCanBusFrame frame;
    const int maxPayload = 8; //Max 8
    writings.truncate(maxPayload);
    frame.setPayload(writings);

    qint32 id = ui->idEdit->displayText().toInt(nullptr, 16);

    frame.setFrameId(id);

    m_canDevice->writeFrame(frame);

}

//----------------------------------------------------------------------------------------------------
void Widget::timerEvent(QTimerEvent* ev){
    if(ev->timerId() == mTimerId){
        this->killTimer(mTimerId);
        mTimerId = -1;
        ui->statusLabel->setText("");
    }
}

//----------------------------------------------------------------------------------------------------
void Widget::writeStatusMsg(QString msg){

    ui->statusLabel->setText(msg);

    if(mTimerId != -1)
        this->killTimer(mTimerId);
    mTimerId = this->startTimer(2000);

}
