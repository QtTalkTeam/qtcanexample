#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QCanBusDevice>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private:
    Ui::Widget *ui;

    QCanBusDevice *m_canDevice;

    QString frameFlags(const QCanBusFrame &frame);
    QByteArray dataFromHex(const QString &hex);
    void writeStatusMsg(QString msg);

    int mTimerId;

protected:
    void closeEvent(QCloseEvent *event);
    void timerEvent(QTimerEvent* ev);

public slots:
    void sendMessageSlot();
    void connectDeviceSlot();
    void disconnectDevice();

    void framesWritten(qint64 count);
    void receiveError(QCanBusDevice::CanBusError error);
    void checkMessages();

};

#endif // WIDGET_H
